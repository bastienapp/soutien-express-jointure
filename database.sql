CREATE DATABASE express_steam;

USE express_steam;

CREATE USER 'Craving0006'@'localhost' IDENTIFIED BY 'ZA^Sg!kvB!4&Rw46S5Spov4FpTn4G837';

GRANT ALL PRIVILEGES ON express_steam.* TO 'Craving0006'@'localhost';

FLUSH PRIVILEGES;

CREATE TABLE game (
	game_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	title VARCHAR(255) NOT NULL,
	release_year YEAR,
	rate FLOAT NOT NULL DEFAULT 0
);

INSERT INTO game (title, release_year, rate)
	VALUES ("Portal", 2008, 8);

INSERT INTO game (title, release_year, rate)
	VALUES ("Tunic", 2022, 10);

INSERT INTO game (title, release_year, rate)
	VALUES ("Super Mario 64", 1994, 9);

SELECT * FROM game;

--ajouter une table category, un jeu peut avoir plusieurs catégories

CREATE TABLE category (
	category_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL
);

INSERT INTO category (name) VALUES ("Puzzle");
INSERT INTO category (name) VALUES ("Plate-forme");
INSERT INTO category (name) VALUES ("FPS");

CREATE TABLE game_category(
	game_id INT,
	category_id INT,
FOREIGN KEY (game_id) REFERENCES game(game_id),
FOREIGN KEY (category_id) REFERENCES category(category_id)
);

-- Portal est un jeu FPS

game_id | title          | release_year | rate |
+---------+----------------+--------------+------+
|       1 | Portal         |         2008 |    8 |
|       2 | Tunic          |         2022 |   10 |
|       3 | Super Mario 64 |         1994 |    9

 category_id | name        |
+-------------+-------------+
|           1 | Puzzle      |
|           2 | Plate-forme |
|           3 | FPS

INSERT INTO game_category (game_id, category_id)
VALUES
(1, 3),
(1, 1);

INSERT INTO game_category (game_id, category_id)
VALUES
(2, 2),
(2, 1);

INSERT INTO game_category (game_id, category_id)
VALUES
(3, 2);

-- Portal est un jeu Puzzle

-- Récupérer Mario (game) et les noms de ses catégories (category et game_category)

SELECT g.title, g.release_year, g.rate, c.name
FROM category c
LEFT JOIN game_category gc ON c.category_id = gc.category_id
LEFT JOIN game g ON gc.game_id =g.game_id
WHERE g.title LIKE '%tunic%';

------------+-------+--------------+------+
| name        | title | release_year | rate |
+-------------+-------+--------------+------+
| Plate-forme | Tunic |         2022 |   10 |
| Puzzle      | Tunic |         2022 |   10 |
