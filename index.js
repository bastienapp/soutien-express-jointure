require("dotenv").config();
const cors = require("cors");
const express = require("express");

const app = express();
app.use(cors({
  origin: 'http://localhost:8080'
}));

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

// ajouter une table category : un jeu peut avoir plusieurs catégories (qui aura un nom)

// modifier la requete GET de games pour récupérer un tableau avec les noms des catégories en plus des infors du jeu

// Importe le contrôleur des livres
const gamesController = require('./controllers/games.controller');

// Utilise le contrôleur pour gérer les routes des livres
app.use('/games', gamesController);

const port = 3001;
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});
